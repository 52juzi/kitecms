<?php
namespace app\common\logic;

use think\Model;
use think\Loader;
use app\common\validate\ArticleValidate;
use app\common\model\Article as ArticleModel;

class Article extends Model
{
    /*
     * 创建文章信息
     *
     * @params $data array
     * @return mix
     */
    public function add($data)
    {
        $validate = Loader::validate('ArticleValidate');
        $result = $validate->scene('add')->check($data);

        if(!$result){
            return $validate->getError();
        }

        // 保存图片 转换通用斜杠符号
        if (isset($data['image'])){
            $data['image'] = str_replace("\\", "/", $data['image']);
        }
        $data['create_at'] = date('Y-m-d H:i:s',time());

        return ArticleModel::add($data);
    }

    /*
     * 更新文章信息
     *
     * @params $data array 文章数据
     * @return mix
    */
    public function modify($data)
    {
        $validate = Loader::validate('ArticleValidate');
        $result = $validate->scene('edit')->check($data);

        if(!$result){
            return $validate->getError();
        }

        // 替换图片格式
        if (!empty($data['image'])) {
            $data['image'] = str_replace("\\", "/", $data['image']);
        }
        $data['update_at'] = date('Y-m-d H:i:s',time());

        return ArticleModel::modify($data);
    }
}