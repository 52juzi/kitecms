<?php
namespace app\common\validate;
use think\Validate;

class CategoryValidate extends Validate
{
    protected $rule = [
        'title' => 'require|max:64',
        'alias' => 'require|alphaDash|max:64',
    ];

    protected $message = [
        'title.require'   =>'栏目名称必须',
        'title.max'       =>'栏目名称最多不能超过64个字符',
        'alias.require'   => '栏目别名必须',
        'alias.alphaDash' => '栏目别名必须为字母和数字,下划线_及破折号-',
        'alias.max'       => '栏目别名最多不能超过64个字符',
    ];
    
    protected $scene = [
        'add'  => ['title', 'alias'],
        'edit' => ['title', 'alias'],
    ];
}