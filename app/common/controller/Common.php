<?php
namespace app\common\controller;

use think\Controller;
use think\Session;
use think\Config;
use app\common\model\Config as ConfigModel;

class Common extends Controller
{
    // 用户UID
    protected $uid;
    // 用户名
    protected $username;
    // 组ID
    protected $gid;

    public function __construct()
    {
        parent::__construct();

        // 获取登陆用户认证信息
        $user = Session::get('user_auth');
        $this->uid      = $user['uid'];
        $this->username = $user['username'];
        $this->gid      = $user['group_id'];
    }

    /** 
     * 空操作
     *
     * @params $dir string 目录
     * @return array
     */
    public function _empty($name)
    {
        return $this->error('访问错误');
    }

    /**
     * 返回信息
     *
     * @params $code int 错误代码
     * @params $msg string 错误信息
     * @params $code array 数据
     * @return json
     */
    protected function response($code, $msg = '', $data = [])
    {
        if (empty($msg)) {
            $msg = \think\Config::get('code.' . $code);
        }

        $data = ['code'=>$code, 'msg'=>$msg, 'data'=>$data];
        return json($data, 200);
    }

    /**
     * 返回数据库配置表中信息
     *
     * @params $key string 配置名
     * @return mix
     */
    public function config($key)
    {
        return ConfigModel::getValue($key);
    }
}
