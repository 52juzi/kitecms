<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Section extends Model
{
    /**
     * 详情
     *
     * @params $id int ID
     * @return array
     */
    public static function getInfo($id)
    {
        return Db::name('book_section')
            ->alias('s')
            ->field('s.*, b.title as book_name')
            ->where('s.id',$id)
            ->join('__BOOK__ b','b.id = s.book_id','LEFT')
            ->find();
    }

    /**
     * 列表
     *
     * @params $map array 查询条件
     * @params $order array|string 排序
     * @params $limit int 每页数量
     * @return array
     */
    public static function getList($map, $order, $limit)
    {
        return Db::name('book_section')
            ->alias('s')
            ->field('s.*, b.title as book_name')
            ->where($map)
            ->join('__BOOK__ b','b.id = s.book_id','LEFT')
            ->order($order)
            ->paginate($limit);
    }

    /**
     * 列表 (不分页)
     *
     * @params $map array 查询条件
     * @params $order array|string 排序
     * @return array
     */
    public static function getAll($map, $order)
    {
        return Db::name('book_section')
            ->alias('s')
            ->field('s.*, b.title as book_name')
            ->where($map)
            ->join('__BOOK__ b','b.id = s.book_id','LEFT')
            ->order($order)
            ->select();
    }

    /**
     * 创建
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('book_section')->insertGetId($data);
    }

    /**
     * 更新
     *
     * @params $data array
     * @return mix
     */
    public static function modify($data)
    {
        return Db::name('book_section')->update($data);
    }

    /**
     * 删除
     *
     * @params $id int ID
     * @return int
     */
    public static function remove($id)
    {
        return Db::name('book_section')->delete($id);
    }
}
