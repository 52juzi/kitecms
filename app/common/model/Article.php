<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Article extends Model
{
    /**
     * 获取文章信息
     *
     * @params $article_id int 文章ID
     * @return array
     */
    public static function getInfo($article_id)
    {
        return Db::name('article')
            ->alias('a')
            ->field('a.*, c.alias as category_alias, c.title as category_name, c.content_template, comment_template, u.username, u.nickname, u.motto, u.avatar')
            ->where('a.id', '=', $article_id)
            ->join('__CATEGORY__ c','a.category_id = c.id','LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->find();
    }

    /**
     * 获取推荐位列表(不分页)
     *
     * @params $map array 查询条件
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public static function getPosition($map, $order, $limit)
    {
        return Db::name('article')
            ->alias('a')
            ->field('a.*, c.alias as category_alias, c.title as category_name, c.list_template, comment_template, u.username, u.nickname, u.motto, u.avatar')
            ->where($map)
            ->join('__CATEGORY__ c','a.category_id = c.id','LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->order($order)
            ->limit($limit)
            ->select();
    }

    /**
     * 文章列表(分页)
     *
     * @params $map array 查询条件
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public static function getList($map, $order, $limit, $params = [])
    {
        return Db::name('article')
            ->alias('a')
            ->field('a.*, c.alias as category_alias, c.title as category_name, c.list_template, u.username, u.nickname, u.motto, u.avatar')
            ->join('__CATEGORY__ c','a.category_id = c.id','LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->where($map)
            ->order($order)
            ->paginate($limit, false, ['query' => $params]);
    }


    /**
     * 获取列表分页页码
     *
     * @params $map array 查询条件
     * @params $limit int 查询数量
     * @return array
     */
    public static function getPage($map, $limit)
    {
        $data = Db::name('article')
            ->where($map)
            ->paginate($limit);

        return $data->render();
    }

    /**
     * 获取上一条信息
     *
     * @params $article_id int 当前ID
     * @return array
     */
    public static function getPrev($article_id)
    {
        return Db::name('article')
            ->where('id', '<', $article_id)
            ->where('status', 1)
            ->order('id desc')
            ->find();
    }

    /**
     * 获取下一条信息
     *
     * @params $article_id int 当前ID
     * @return array
     */
    public static function getNext($article_id)
    {
        return Db::name('article')
            ->where('id', '>', $article_id)
            ->where('status', 1)
            ->order('id asc')
            ->find();
    }

    /**
     * 创建文章信息
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('article')->insertGetId($data);
    }

    /**
     * 更新文章信息
     *
     * @params $data array
     * @return mix
     */
    public static function modify($data)
    {
        return Db::name('article')->update($data);
    }

    /**
     * 推荐
     *
     * @params $id
     * @return mix
     */
    public static function recommend($id, $value)
    {
        return Db::name('article')->where('id',$id)->setField('recommend', $value);
    }

    /**
     * 热门
     *
     * @params $id
     * @return mix
    */
    public static function hot($id ,$value)
    {
        return Db::name('article')->where('id',$id)->setField('hot', $value);
    }

    /**
     * 设置封面图片
     *
     * @params $image
     * @return mix
     */
    public static function setImage($id ,$value)
    {
        return Db::name('article')->where('id',$id)->setField('image', $value);
    }

    /**
     * 设置访问次数
     *
     * @params $image
     * @return mix
     */
    public static function setPv($article_id, $times)
    {
        return Db::name('article')->where('id',$article_id)->setField('pv', $times);
    }

    /**
     * 审核文章
     *
     * @params $id int 文章ID
     * @params $value int 0 待审 1通过
     * @return mix
     */
    public static function setStatus($id ,$value)
    {
        return Db::name('article')->where('id',$id)->setField('status', $value);
    }

    /**
     * 删除
     *
     * @params $id
     * @return mix
     */
    public static function remove($id)
    {
        return Db::name('article')->delete($id);
    }

    /**
     * 查询PV数量
     *
     * @params $id int 文章ID
     * @return mix
     */
    public static function getPv($id)
    {
        return Db::name('article')->where('id',$id)->value('pv');
    }

    /**
     * 查询关键词
     *
     * @params $id int 文章ID
     * @return mix
     */
    public static function getKeywords($id)
    {
        return Db::name('article')->where('id',$id)->value('keywords');
    }

    /**
     * 统计用户文章总数
     *
     * @params $uid int UID
     * @return int
     */
    public static function getArticleSum($uid)
    {
        return Db::name('article')->where('uid',$uid)->count();
    }

    /**
     * 统计用户文章总访问量
     *
     * @params $uid int UID
     * @return int
     */
    public static function getArticlePvSum($uid)
    {
        return Db::name('article')->where('uid',$uid)->sum('pv');
    }
}