<?php
namespace app\common\model;

use think\Model;
use think\Db;

class AuthAccess extends Model
{
    public static function store($group_id,$data)
    {
        if (!empty($data)){
            $arr = array();
            foreach ($data as $v){
                $x['group_id'] = $group_id;
                $x['rule_id'] = $v;
                $arr[] = $x;
            }
            //写入前先清理该组所有权限
            db('auth_access')->where('group_id', $group_id)->delete();
            return Db::name('auth_access')->insertAll($arr);
        } else {
            //清理该组所有权限
            db('auth_access')->where('group_id', $group_id)->delete();
            return true;
        }
    }
    
    public static function getAccess($group_id)
    {
        $access = Db::name('auth_access')->where('group_id', $group_id)->select();
        $arr = array();
        foreach ($access as $v) {
            $arr[] = $v['rule_id'];
        }
        return $arr;
    }
    
    public static function remove($group_id)
    {
        return db('auth_access')->where('group_id', $group_id)->delete();
    }
}
