<?php
namespace app\common\taglib;

use think\template\TagLib;

class Book extends TagLib{
    /**
     * 定义全局标签列表
     *
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'list'     => ['attr' => 'name,category_id,order,limit', 'close' => 1], //列表
        'page'     => ['attr' => 'category_id,limit', 'close' => 0], //列表分页
        'position' => ['attr' => 'name,category_id,recommend,hot,order,limit', 'close' => 1], //获取推荐位列表
    ];

    /**
     * 图书列表
     *
     */
    public function tagList($tag, $content)
    {
        $category_id = $tag['category_id'];
        $order       = empty($tag['order']) ? 'id desc' : $tag['order'];
        $limit       = empty($tag['limit']) ? 10 : $tag['limit'];
        $name        = $tag['name'];
        $parse = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\service\Book(); ';
        $parse .= '$__LIST__ = $__MODEL__->getList("' . $category_id . '","' . $order . '",' . $limit . ');';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    /**
     * 图书分页
     *
     */
    public function tagPage($tag, $content)
    {
        $category_id = $tag['category_id'];
        $limit       = $tag['limit'];
        $parse = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\service\Book(); ';
        $parse .= '$__PAGE__ = $__MODEL__->getPage("' . $category_id . '",' . $limit . ');';
        $parse .= 'echo $__PAGE__;';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 推荐位列表
     *
     */
    public function tagPosition($tag, $content)
    {
        $category_id = empty($tag['category_id']) ? 'null' : $tag['category_id'];
        $recommend   = empty($tag['recommend']) ? 'null' : $tag['recommend'];
        $hot         = empty($tag['hot']) ? 'null' : $tag['hot'];
        $order       = empty($tag['order']) ? 'id desc' : $tag['order'];
        $limit       = empty($tag['limit']) ? 10 : $tag['limit'];
        $name        = $tag['name'];
        $parse = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\service\Book(); ';
        $parse .= '$__LIST__ = $__MODEL__->getPosition("';
        $parse .= $category_id . '",';
        $parse .= $recommend . ',';
        $parse .= $hot . ',"';
        $parse .= $order . '",';
        $parse .= $limit;
        $parse .= ');';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }
}