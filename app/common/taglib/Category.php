<?php
namespace app\common\taglib;

use think\template\TagLib;

class Category extends TagLib{
    /**
     * 定义全局标签列表
     *
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'list' => ['attr' => 'name,child,id,order,limit,class', 'close' => 1],

    ];

    /**
     * 分类列表标签
     *
     */
     
    public function tagList($tag, $content)
    {
        $name  = $tag['name'];
        $child = empty($tag['child']) ? 'child' : $tag['child'];
        $id    = empty($tag['id']) ? 'null' : $tag['id'];
        $order = empty($tag['order']) ? 'id desc' : $tag['order'];
        $limit = empty($tag['limit']) ? 10 : $tag['limit'];
        $class = empty($tag['class']) ? 'active' : $tag['class'];
        $parse = '<?php ';
        $parse .= '$__MODEL__ = new \app\common\service\Category(); ';
        $parse .= '$__LIST__ = $__MODEL__->getListLayer("' . $id . '","' . $child. '","' . $order . '",' . $limit . ',"' . $class . '");';
        $parse .= ' ?>';
        $parse .= '{volist name="__LIST__" id="' . $name . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

}