<?php
namespace app\common\service;

use app\common\model\Config as ConfigModel;

class Image
{
    // 图片保存目录
    private $savePath = null; 

    // 图片裁剪配置
    private $cropConfig = null;
    
    // 图片缩略图保存目录 
    private $thumbUpload = null;

    private static $_instance;

    private function __construct()
    {

    }

    private function __clone() 
    {

    }

    public static function instance()
    {
        if (! (self::$_instance instanceof self) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 生成缩略图
     *
     * @params $file string 图片路径
     * @params $width int 图片高度
     * @params $height int 图片宽度
     * @return string
     */
    public function thumb($file = '', $width = 150, $height = 150)
    {
        // $file = 'http://www.dms.com/upload/image/20170421/0d7daff973c001bf797f0e62b0cfb7d9.png'; // 测试用图
        // 解析图片URL 
        $paras = str_replace(getDomain(), "", $file);
        $file = ROOT_PATH . ltrim($paras, '/');

        // 获取图片信息
        $sf = new \SplFileInfo($file);

        if (!$sf->isFile()) {
           // 如果图片文件不存在，加载默认图片
           $file = ROOT_PATH . 'static/img/nopic.jpg';
        }

        // 图片名称解散为图片名+后缀
        $exp = explode('.', basename($file));

        // 缩略图保存目录 
        $thumb_upload = ROOT_PATH . 'upload/thumb/';
        if (!is_dir($thumb_upload)) {
            mkdir($thumb_upload, 0777);
        }
        // 生成缩略图命名
        $thumbName = $exp[0] . '-' . $width . 'x' . $height . '.' . $exp[1];
        $thumb = $thumb_upload . $thumbName;

        // 缩略图是否存在 不存在则生成
        $tn = new \SplFileInfo($thumb);
        if (!$tn->isFile()) {
            // 获取生成缩略图方案策略
            $getType    = ConfigModel::getValue('thumb');
            $thumb_type = empty($getType) ? 1 : $getType;
            // 打开图片
            $image = \image\Image::open($file);
            $create = $image->thumb($width,$height,$thumb_type)->save($thumb);
        }

        // 拼接绝对路径并返回
        return str_replace("\\", "/", getDomain() . 'upload/thumb/' . $thumbName);
    }

    /**
     * 编辑器上传图片生成水印
     *
     * @params $file string 图片路径
     * @return string
     */
    public function ueditorWater($file)
    {
        $config = json_decode(ConfigModel::getValue('water'), true);

        // 如果没有开启水印功能
        if ($config['open'] == 0) {
            return false;
        }

        $logo       = ROOT_PATH . $config['logo']; //自定义配置水印logo
        $defaltLogo = ROOT_PATH . 'static/img/water_logo.png'; //系统默认水印logo
        if (!is_file($logo) && !is_file($defaltLogo)) {
            return false;
        }

        //生成水印图
        $tn = new \SplFileInfo($file);
        if ($tn->isFile()) {
            // 获取生成水水印图片方案策略
            $position   = empty($config['position']) ? 1 : $config['position'];
            $quality    = empty($config['quality']) ? 100 : $config['quality'];
            $scan       = empty($config['scan']) ? true : $config['scan'];
            $logo       = !is_file($logo) ? $defaltLogo : $logo;

            // 打开图片并生成
            $image = \image\Image::open($file);
            return $image->water($logo, $position, $quality, $scan)->save($file);
        }
    }
}