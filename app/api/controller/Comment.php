<?php
namespace app\api\controller;

use think\Request;
use app\api\controller\ApiBase;
use app\common\logic\Comment as CommentLogic;

class Comment extends ApiBase
{
    /**
     * 添加评论
     *
     * @params $data array 评论数据
     * @return json
     */
    public function add()
    {
        if (!$this->uid) {
            $this->error('请先登录');
        }

        $data = Request::instance()->param();

        // 判断评论内容是否为空
        if (empty($data['content'])) {
            return $this->response(1701);
        }

        $data['from_uid'] = $this->uid;
        $commentLogic = new CommentLogic();
        $result = $commentLogic->add($data);

        return $this->response(is_numeric($result) ? 200 : 201);
    }

    /**
     * 喜欢+1
     *
     * @params $id int 评论ID
     * @return json
     */
    public function like()
    {
        if (!$this->uid) {
            $this->error('请先登录');
        }

        $id = Request::instance()->param('id');
        $commentLogic = new CommentLogic();
        $result = $commentLogic->setLike($id);

        return $this->response(is_numeric($result) ? 200 : 201);
    }
}
