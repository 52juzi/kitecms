<?php
namespace app\index\controller;

use think\Request;
use app\common\model\Tag as TagModel;
use app\common\service\Tag as TagLogic;
use app\index\controller\Base;

class Tag extends Base
{
    private $_data = [];

    public function _initialize()
    {
        $this->_data = $this->getData();
    }

    protected function getData()
    {
        return TagModel::getInfo(Request::instance()->param('id'));
    }

    public function read()
    {
        if (!empty($this->_data)) {
            // Tag信息 数组键名转换为变量 并传值到模板
            foreach ($this->_data as $k => $v) {
                $this->assign($k,$v);
            }
        } else {
            $this->error('您访问的页面不存在!');
        }

        // 查询文章列表
        $tagLogic = new TagLogic();
        $data = $tagLogic->getArticleList($this->_data['id'], 'id desc', 10);

        // 传值到模板页
        $this->assign('list', $data['list']);
        $this->assign('page', $data['page']);

        return $this->fetch($this->template('tag_tpl'));
    }
}
