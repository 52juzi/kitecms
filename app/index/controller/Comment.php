<?php
namespace app\index\controller;

use think\Request;
use app\common\service\Article as ArticleLogic;
use app\common\service\Comment as CommentLogic;
use app\index\controller\Base;

class Comment extends Base
{

    private $_data = [];
    private $_id   = '';

    function _initialize()
    {
        $this->_id = $this->getArticleId();
        $this->_data = $this->getData();
    }

    protected function getData()
    {
        $articleLogic = new ArticleLogic();
        $articleLogic->setPv($this->_id);

        return $articleLogic->getInfo($this->_id);
    }

    protected function getArticleId()
    {
        return Request::instance()->param('id');
    }

    public function read()
    {
        // 文章信息
        if (!empty($this->_data)) {
            // 数组键名转换为变量 并传值到模板
            foreach ($this->_data as $k => $v) {
                $this->assign($k, $v);
            }
        } else {
            $this->error('您访问的页面不存在!');
        }

        // 评论列表
        $commentLogic = new CommentLogic();
        $data = $commentLogic->getList($this->_id, 'id desc', $this->config('comment_limit'));

        $this->assign('list', $data['list']);
        $this->assign('page', $data['page']);

        // 模版 优先使用栏目独立模版配置
        return $this->fetch($this->template('article_comment_tpl', $this->_data['comment_template']));
    }
}
