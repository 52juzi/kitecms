<?php
namespace app\admin\controller;

use think\Request;
use think\Db;
use app\common\model\Comment as CommentModel;

class Comment extends Admin
{

    public function index()
    {
        $search = Request::instance()->param();

        $map = array();
        //搜索条件
        if (isset($search['keywords']) && $search['keywords'] != '') {
            $map['c.content'] = array('like', '%'.$search['keywords'].'%');
        }
        if (isset($search['status']) && $search['status'] != '') {
            $map['c.status'] = $search['status'];
        }

        $data = CommentModel::getList($map, 'id desc', 10);
        $this->assign('data', $data);
        $this->assign('search', $search);

        return $this->fetch('admin/comment/index');
    }

    public function handle()
    {
        $data = Request::instance()->param();
        // print_r($data);
        switch ($data['type']) {
            case 'delete':
                foreach ($data['ids'] as $v) {
                    $result = CommentModel::remove($v, 0);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
            case 'change':
                foreach ($data['ids'] as $v) {
                    $result = CommentModel::setStatus($v, 1);
                }

                if ($result !== false) {
                    return $this->response(200);
                }
                break;
        }
        return $this->response(201);
    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $data = CommentModel::getInfo($id);

        $this->assign('data', $data);
        return $this->fetch('admin/comment/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $result = CommentModel::modify($data);

        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(201,$result);
        }
    }

    public function remove()
    {
        if (Request::instance()->isAjax()) {
            $result = CommentModel::remove(Request::instance()->param('id'));
            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }
    }

}
