<?php
namespace app\admin\controller;

use app\admin\controller\Admin;
use think\Request;
use app\common\model\AuthGroup;
use app\common\model\User as UserModel;
use app\common\logic\User as UserLogic;

class User extends Admin
{
    protected $userLogic;

    public function _initialize()
    {
        $this->userLogic = new UserLogic();
    }

    public function index()
    {
        $search = Request::instance()->param();

        $map = $params = array();
        if (isset($search['keywords']) && $search['keywords'] != '') {
            $params = ['keywords' => $search['keywords']];
            $map['username'] = $map['email'] = $map['phone'] = array('like', '%'.$search['keywords'].'%');
        }
        $data = UserModel::getList($map, 'uid desc', 10, $params);

        $this->assign('data',$data);
        $this->assign('search', $search);
        return $this->fetch('admin/user/index');
    }

    public function create()
    {
        $data['groupList'] = AuthGroup::getList();
        $this->assign('data', $data);
        return $this->fetch('admin/user/admin/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        //检测用户名
        $checkUsername = UserModel::checkUser(['username'=>$data['username']]);
        if ($checkUsername) {
            return $this->response(1101);
        }
        //检测邮箱
        $checkEmail = UserModel::checkUser(['email'=>$data['email']]);
        if ($checkEmail) {
            return $this->response(1102);
        }
        //检测电话号码
        $checkPhone = UserModel::checkUser(['phone'=>$data['phone']]);
        if ($checkPhone) {
            return $this->response(1103);
        }
        //注册
        $ret = $this->userLogic->add($data);
        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201, $ret);
        }
    }

    public function edit()
    {
        $uid = Request::instance()->param('uid');
        $data['groupList'] = AuthGroup::getList();
        $data['userInfo'] = UserModel::getInfo($uid);
        $this->assign('data',$data);
        return $this->fetch('admin/user/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        //检测用户
        $userInfo = UserModel::getInfo($data['uid']);
        if($userInfo['username']!=$data['username']){
            $checkUsername = UserModel::checkUser(['username'=>$data['username']]);
            if($checkUsername){
                return $this->response(1101);
            }
        }
        //检测邮箱
        if($userInfo['email']!=$data['email']){
            $checkEmail = UserModel::checkUser(['email'=>$data['email']]);
            if($checkEmail){
                return $this->response(1102);
            }
        }
        //检测电话
        if($userInfo['phone']!=$data['phone']){
            $checkPhone = UserModel::checkUser(['phone'=>$data['phone']]);
            if($checkPhone){
                return $this->response(1103);
            }
        }

        $ret = $this->userLogic->modify($data);
        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201, $ret);
        }
    }

    public function remove()
    {
        $data = Request::instance()->param();
        return UserModel::remove($data);
    }
    
}
