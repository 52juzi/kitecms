<?php
namespace app\admin\controller;

use think\Request;
use think\Db;
use app\common\model\Link as LinkModel;

class Link extends Admin
{

    public function index()
    {
        $data = LinkModel::getList();
        $this->assign('data', $data);
        return $this->fetch('admin/link/index');
    }

    public function create()
    {
        return $this->fetch('admin/link/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $result = LinkModel::add($data);
        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(201);
        }
    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $info = LinkModel::getInfo($id);
        $this->assign('data', $info);
        return $this->fetch('admin/link/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $result = LinkModel::edit($data);
        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(201);
        }
    }

    public function remove()
    {
        if (Request::instance()->isAjax()) {
            $id = Request::instance()->param('id');
            $result = LinkModel::remove($id);
            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }
    }

}
